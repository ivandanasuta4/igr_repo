<html>
    <head>
        <!-- meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>IGR Repo - Upload APK</title>
        <!-- plugin css -->
        <link rel="stylesheet" href="{{ asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css')}}">
        <link rel="stylesheet" href="{{ asset('css/style.css')}}">
        <!-- bootstrap -->
        <link rel="stylesheet" href=" {{ asset('css/bootstrap/bootstrap.min.css') }}">
        <!-- favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.png')}}" />
    </head>
    <body>
        <div class="container-scroller">
            <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                    <a class="navbar-brand brand-logo" href="">
                        <img src="{{ asset('images/indogrosir.jpg')}}" alt="logo"/>
                    </a>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-stretch">
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item nav-profile dropdown">
                            <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                            <div class="nav-profile-img">
                                <img src="images/faces/face1.jpg" alt="image">
                                <span class="availability-status online"></span>             
                            </div>
                            <div class="nav-profile-text">
                                <p class="mb-1 text-white">Support Programmer</p>
                            </div>
                            </a>
                            <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="mdi mdi-cached mr-2 text-success"></i>
                                Activity Log
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">
                                <i class="mdi mdi-logout mr-2 text-primary"></i>
                                Signout
                            </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="container-fluid page-body-wrapper">
                <!-- partial:partials/_sidebar.html -->
                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">
                        <li class="nav-item mt-4">
                            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                            <span class="menu-title">Configuration</span>
                            <i class="menu-arrow"></i>
                            <i class="mdi mdi-crosshairs-gps menu-icon"></i>
                            </a>
                            <div class="collapse" id="ui-basic">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Apks</a></li>
                            </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- partial -->
                <div class="main-panel">
                    <div class="content-wrapper">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @elseif ($message = Session::get('warning'))
                            <div class="alert alert-danger alert-block" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <div class="page-header">
                            <h3 class="page-title">
                            <span class="page-title-icon bg-gradient-dark text-white mr-2">
                                <i class="mdi mdi-crosshairs-gps"></i>                 
                            </span>
                            Upload Your New APK
                            </h3>
                            <nav aria-label="breadcrumb">
                                <button class="btn btn-block btn-lg btn-gradient-dark" id="upload-apk">+ Upload Now</button>
                                <button class="btn btn-block btn-lg btn-gradient-dark" id="btn-cancel-upload">- Cancel Upload</button>
                            </nav>
                        </div>
                        <div class="row">
                            <div class="col-12 grid-margin stretch-card" id="form-upload">
                                <div class="card">
                                    <div class="card-body">
                                    <h4 class="card-title">Form Upload Application</h4>
                                    <form class="forms-sample" method="POST" enctype="multipart/form-data" id="form-upload-apk" action="{{url('/upload-apk')}}">
                                    {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>File upload</label>
                                            <input type="file" name="apk" accept=".apk" class="file-upload-default" required>
                                            <div class="input-group col-xs-12">
                                                <input type="file" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                                <span class="input-group-append">
                                                    <button class="file-upload-browse btn btn-gradient-dark" type="button">Upload</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi Aplikasi</label>
                                            <input type="text" name="deskripsi" class="form-control">
                                        </div>
                                        
                                        <button type="submit" class="btn btn-gradient-dark mr-2" id="btn-upload-apk">Submit</button>
                                        <button class="btn btn-light" id="cancel-upload">Cancel</button>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                <h4 class="card-title">List Application</h4>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>
                                                Nama Aplikasi
                                            </th>
                                            <th>
                                                License
                                            </th>
                                            <th>
                                                Version
                                            </th>
                                            <th>
                                                Size (KB)
                                            </th>
                                            <th>
                                                Ditambahkan Pada
                                            </th>
                                            <th>
                                                Aksi
                                            </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($dataApk as $data)
                                            <tr>
                                                <td style="vertical-align:middle;">
                                                    <!-- <img src="images/faces/face1.jpg" class="mr-2" alt="image"> -->
                                                    <i class="mdi mdi-android-debug-bridge"></i>
                                                    {{$data->name}}
                                                </td>
                                                <td style="vertical-align:middle;">
                                                    {{$data->license}}
                                                </td>
                                                <td style="vertical-align:middle;">
                                                    {{$data->version}}
                                                </td>
                                                <td style="vertical-align:middle;">
                                                    <!-- <label class="badge badge-gradient-success">DONE</label> -->
                                                    {{$data->size}}
                                                </td>
                                                <td style="vertical-align:middle;">
                                                    {{$data->created_at}}
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-success"><i class="mdi mdi-settings"></i></button>
                                                    <button data_id ="{{$data->id}}" class="btn btn-sm btn-danger" id="delete-apk"><i class="mdi mdi-delete"></i></button>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- content-wrapper ends -->
                    <!-- partial:partials/_footer.html -->
                    <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap Dash</a>. All rights reserved.</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
                    </div>
                    </footer>
                    <!-- partial -->
                </div>
                <!-- main-panel ends -->
            </div>
        </div>
        <!-- plugins:js -->
        <script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>
        <script src="{{asset('vendors/js/vendor.bundle.addons.js')}}"></script>
        <!-- endinject -->
        <!-- Plugin js for this page-->
        <!-- End plugin js for this page-->
        <!-- inject:js -->
        <script src="{{asset('js/off-canvas.js')}}"></script>
        <script src="{{asset('js/misc.js')}}"></script>
        <!-- endinject -->
        <!-- Custom js for this page-->
        <script src="{{asset('js/dashboard.js')}}"></script>
        <script src="{{asset('js/file-upload.js')}}"></script>
        <!-- End custom js for this page-->

        <script>
            $(document).ready(function(){
                $('#form-upload').hide();
                $('#btn-cancel-upload').hide();

                $(document).on('click','#upload-apk',function(){
                    $('#form-upload').show();
                    $('#upload-apk').hide();
                    $('#btn-cancel-upload').show();
                });

                $(document).on('click','#cancel-upload',function(){
                    $('#form-upload').hide();
                    $('#upload-apk').show();
                    $('#btn-cancel-upload').hide();
                });

                $(document).on('click','#btn-cancel-upload',function(){
                    $('#btn-cancel-upload').hide();
                    $('#form-upload').hide();
                    $('#upload-apk').show();
                });
                
                $(document).on('submit','#form-upload-apk',function(){
                    $('#btn-upload-apk').empty().prop('disabled', true).append('<i class="fas fa-spinner fa-spin"></i> Uploading APK');
                });

                $(document).on('click','#delete-apk',function(){
                    var id = $(this).attr('data_id');
                    
                    $.ajax({
                        type: "POST",
                        url: "{{url('/delete-apk')}}",
                        data: {
                            id: id
                        },
                        headers: {
                            ''
                        }
                    })
                });

            });
        </script>
    </body>
</html>