<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('login-form','AuthController@loginForm')->name('login-form');


Route::group([`prefix` => 'auth'], function(){
    Route::post('login','AuthController@login');
    Route::post('signup','AuthController@signup');

    Route::post('refresh_token','AuthController@userRefreshToken');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('get_data_member','AuthController@get_data_member');
        Route::get('logout','AuthController@logout');
        Route::post('add_or_update_cart','TMIKlikController@addUpdateCart');
        Route::get('get_cart_pb_klik','TMIKlikController@getCartPBKlik');
        Route::post('sync_data_download','TMIKlikController@syncDataDownload');
        Route::post('sync_data_upload','TMIKlikController@syncDataUpload');
        // Route::get('user','AuthController@user');
        // Route::post('refresh_token','AuthController@userRefreshToken');
    });
});
