<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('send-mail','AuthController@sendMail');

//Upload APK
Route::get('/','DummyController@index');
Route::post('/upload-apk','DummyController@uploadApk');
Route::post('/update-apk','DummyController@updateApk');
Route::post('/delete-apk','DummyController@deleteApk');


Route::group(['middleware' => 'auth:api'], function(){
    Route::get('get_user','AuthController@get_user');
    Route::get('get_detail_user','AuthController@get_detail_user');
    Route::get('get_data_member','AuthController@get_data_member');
    // Route::get('logout','AuthController@logout');
    // Route::get('user','AuthController@user');
});
