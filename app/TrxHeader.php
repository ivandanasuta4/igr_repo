<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrxHeader extends Model
{
    //
    protected $table = 'trx_headers';
    protected $primaryKey = 'id';
}
