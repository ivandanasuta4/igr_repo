<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrxDetail extends Model
{
    //
    protected $table = 'trx_details';
    protected $primaryKey = 'id';
}
