<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusProdMast extends Model
{
    //
    protected $table = 'status_product_masters';
    protected $primaryKey = 'id';
}
