<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OClient extends Model
{
    //
    protected $table = 'oauth_clients';
    protected $primaryKey = 'id';
}
