<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBarcode extends Model
{
    //
    protected $table = 'user_barcodes';
    protected $primaryKey = 'id';
}
