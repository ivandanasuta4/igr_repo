<?php

namespace App\Http\Controllers;

use App\Apk;
use Illuminate\Http\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DummyController extends Controller
{
    //
    public function index()
    {
        $dataApk = Apk::all();   
        return view('storage.storage',compact('dataApk'));
    }

    public function uploadApk(Request $request)
    {
        $apk = $request->file('apk');

        $apk_name = $apk->getClientOriginalName();
        $apk_size = $apk->getSize();
        $apk_type = $apk->getMimeType();
        $apk_desc = $request->deskripsi;

        $existsAPK = Apk::where('name','=',$apk_name)->first();

        if(!empty($existsAPK))
        {
            return redirect()->back()->with('warning','Nama Aplikasi sudah digunakan');
        }
        else
        {
            $apk_path = $apk->move(base_path('public/fdroid/repo/'),str_replace(' ','_',$apk->getClientOriginalName()));

            Apk::create([
                'name' => $apk_name,
                'desc' => $apk_desc,
                'icon' => '',
                'path_file' => $apk_path->getRealPath(),
                'size' => $apk_size,
                // 'license' => $apk_license,
                // 'version' => $apk_version,
            ]);

            return redirect()->back()->with('success','Aplikasi terbaru anda telah ditambahkan');
        }
    }

    public function updateApk()
    {
        return redirect()->back()->with('success','Update Aplikasi terbaru anda telah ditambahkan');
    }

    public function deleteApk(Request $request)
    {
        return redirect()->back()->with('success','Delete Aplikasi telah berhasil dilakukan');
    }
}
