<?php

namespace App\Http\Controllers;

use App\User;
use App\StatusProdMast;
use App\PBStatus;
use App\PaymentMethod;
use App\UserProduct;
use App\Barcode;
use App\UserBarcode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TMIKlikController extends Controller
{
    public function addUpdateCart(Request $request)
    {
        $cart = $request->cart;

        foreach($cart as $c)
        {
            $product_code = $c['product_code'];
            $quantity = $c['quantity'];

            $cart_items['plu'] = $product_code;
            $cart_items['qty'] = $quantity;

            $arr_cart[] = $cart_items;
        }

        // return response()->json($arr_cart);

        $member_code = Auth::user()->member_code; 
        
        $data = [
            'member_code' => $member_code,
            'cart_items' => $arr_cart
        ];

        // return response()->json($data);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://klikigrsim.mitraindogrosir.co.id/api/tmi/cart/update",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $response_decode = json_decode($response);

        Cache::put('data'.$member_code,$response_decode);

        return response()->json($response_decode);
        $err = curl_error($curl);

        curl_close($curl);
    }

    public function getCartPBKlik()
    {
        $member_code = Auth::user()->member_code;

        $cache_data = Cache::get('data'.$member_code);

        if($cache_data != null)
        {
            $data = [
                'status' => 1,
                'message' => 'Keranjang tersedia',
                'data' => $cache_data
            ];
        }
        else
        {
            $data = [
                'status' => 0,
                'message' => 'Keranjang tidak tersedia'
            ];
        }

        return response()->json($data);
    }

    public function syncDataDownload()
    {
        ini_set('max_execution_time', '1000');

        $user_id = Auth::user()->id;

        $data_user = User::join('branches','branches.id','users.branch_id')
        ->join('tmi_types','tmi_types.id','users.tmi_type_id')
        ->select('branches.code as branch_code',
        'tmi_types.code as tmi_code',
        'users.member_code',
        'users.name',
        'users.store_name',
        'users.phone_number',
        'users.address',
        'users.email',
        'users.limit_free_plu')
        ->where('users.id','=',$user_id)
        ->get();

        // return response()->json($data_user);

        $data_status_prodmast = StatusProdMast::all();
        $data_pb_status = PBStatus::all();
        $data_payment_methods = PaymentMethod::all();
        $data_product = UserProduct::leftjoin('products','user_products.product_id','=','products.id')
        ->leftjoin('categories','categories.id','=','products.category_id')
        ->rightjoin('ownerships','ownerships.id','=','user_products.ownership_id')
        ->where('user_products.user_id','=',$user_id)
        ->select('user_products.product_id',
        'user_products.ownership_id',
        'products.plu as product_code',
        'products.description as product_name',
        'categories.min_price',
        'categories.max_price',
        'categories.rec_price as suggest_price',
        'user_products.stock',
        'products.unit as unit_igr',
        'user_products.unit as unit_tmi',
        'products.min_order',
        'products.min_qty',
        'products.max_qty',
        'user_products.flag_freeplu',
        'user_products.id as user_product_id',
        'user_products.unit',
        'user_products.cost',
        'user_products.price',
        'ownerships.ownership',
        'products.status_product_master_id',
        'products.conversion',
        'products.frac_igr',
        'products.frac_tmi',
        'user_products.deleted_at')
        ->groupBy('user_products.id')
        // ->limit(10)
        ->get();

        $collect_prod = collect($data_product);
        $filter_prod_id = $collect_prod->where('product_id','!=',null)->pluck('product_id');
        $filter_user_prod_id = $collect_prod->where('product_id','=',null)->pluck('id');

        $barcode = Barcode::join('products','products.id','=','barcodes.product_id')
        ->whereIn('barcodes.product_id',$filter_prod_id)
        ->select('products.plu as product_code','barcodes.barcode')
        ->get();

        $user_barcode = UserBarcode::join('user_products','user_products.id','=','user_barcodes.user_product_id')
        ->where('user_product_id','=',$filter_user_prod_id)
        ->select('user_products.plu as product_code','user_barcodes.barcode')
        ->get();


        $data_barcode = collect($barcode,$user_barcode);
        // $product_id = UserProduct::where('user_id','=',$user_id)
        // ->select('product_id','id')
        // ->get();

        // $prod = collect($product_id);

        // $collapsed_product_id = $prod->keyBy('product_id');
        // $collapsed_user_prod_id = $prod->keyBy('user_product_id');

        // $keys_prod = $collapsed_product_id->keys();
        // $key_user_prod = $collapsed_user_prod_id->keys();

        // if($keys_prod != null)
        // {
        //     $barcode = Barcode::join('products','products.id','=','barcodes.product_id')
        //     ->whereIn('barcodes.product_id',$keys_prod)
        //     ->select('products.plu as product_code','barcodes.barcode')
        //     ->get();
        // }
        // else
        // {
        //     $barcode = UserBarcode::join('user_products','user_products.id','=','user_barcodes.user_product_id')
        //     ->where('user_product_id','=',$key_user_prod)
        //     ->select('user_products.plu as product_code','user_barcodes.barcode')
        //     ->get();
        // }

        // foreach($data_product as $prod)
        // {
        //     if($prod->product_id != null)
        //     {
        //         $barcode = Barcode::join('products','products.id','=','barcodes.product_id')
        //         ->where('barcodes.product_id','=',$prod->product_id)
        //         ->select('products.plu as product_code','barcodes.barcode')
        //         ->get();
        //     }
        //     else
        //     {
        //         $barcode = UserBarcode::join('user_products','user_products.id','=','user_barcodes.user_product_id')
        //         ->where('user_product_id','=',$prod->user_product_id)
        //         ->select('user_products.plu as product_code','user_barcodes.barcode')
        //         ->get();
        //     }

        //     foreach($barcode as $bar)
        //     {
        //         $arr_barcode[] = $bar;
        //     }
       // }

        // $data_barcode = $barcode;

        $data_prodmast['data_master_product'] = $data_product;
        $data_prodmast['data_master_barcode'] = $data_barcode;

        $response['status'] = 1;
        $response['error_message'] = "Sinkron berhasil";
        $response['message'] = "Berhasil mencadangkan data!";
        $response['data_user'] = $data_user;
        $response['data_prodmast'] = $data_prodmast;
        $response['data_status_product_master'] = $data_status_prodmast;
        $response['data_payment_methods'] = $data_payment_methods;
        $response['data_pb_status'] = $data_pb_status;

        return response()->json($response, 200);
    }

    public function syncDataUpload()
    {
        $response['status'] = 1;
        $response['message'] = "Sinkron berhasil";

        return response()->json($response, 200);
    }
}
