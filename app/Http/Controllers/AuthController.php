<?php

namespace App\Http\Controllers;

use App\User;
use App\OClient;
use App\Product;
use App\Operator;
use App\Ownership;
use App\PaymentMethod;
use App\PBStatus;
use App\Promotion;
use App\StatusProdMast;
use App\UserProduct;
use App\Barcode;
use App\UserBarcode;
use App\ShiftReport;
use App\TrxHeader;
use App\TrxDetail;
use DB;
use Mail;

use App\Mail\MyTestMail;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class AuthController extends Controller
{
    //
    public function loginForm()
    {
        return view('auth.login');
    }

    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|min:6',
        ]);

        if($validator->fails())
        {
            return response()->json(['error'=> $validator->errors()],401);
        }

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $user->save();

        return response()->json([
            'message' => 'Register Berhasil!'
        ], 201);
    }

    // public function login(Request $request)
    // {
    //     $request->validate([
    //         'email' => 'required|string',
    //         'password' => 'required|string'
    //     ]);

    //     $credentials = request(['email','password']);

    //     if(!Auth::attempt($credentials))
    //     {
    //         return response()->json([
    //             'message' => 'Unauthorized'
    //         ], 401);
    //     }
    //     else
    //     {

    //         $user = $request->user();

    //         // dd($user->createToken('Personal Access Token'));

    //         $tokenResult = $user->createToken('Personal Access Token')->accessToken;
    //         // dd($tokenResult);
    //         $token = $tokenResult;

    //         // $token->save();

    //         return response()->json([
    //             'access_token' => $token,
    //             'token_type' => 'Bearer',
    //             'message' => 'Login Berhasil!'
    //         ]);
    //     }
    // }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        if(Auth::attempt(['email' => $request->email,
        'password' => $request->password]))
        {
            $oClient = OClient::where('password_client',1)->first();
            // dd($oClient);
            return $this->getTokenAndRefreshToken($oClient, $request->email, $request->password);
        }
        else
        {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Logout Berhasil!'
        ]);
    }

    public function user(Request $request)
    {
        return response()->json($request->user()); 
    }

    public function get_detail_user()
    {
        $user =  Auth::user();

        $user = $user->makeHidden(['id','password']);

        $response['status'] = true;
        $response['message'] = 'Profile User';
        $response['data'] = $user;

        return response()->json($response, 200);
    }

    public function get_data_member()
    {
        ini_set('max_execution_time', 1000);

        $user_id = Auth::user()->id;
        // dd($user_id);
        $data_statusProdMast = StatusProdMast::all();
        // dd($data_statusProdMast);
        $data_product = UserProduct::leftjoin('products','user_products.product_id','=','products.id')
        ->leftjoin('categories','categories.id','=','products.category_id')
        ->rightjoin('ownerships','ownerships.id','=','user_products.ownership_id')
        ->where('user_products.user_id','=',$user_id)
        ->select('user_products.product_id',
        'user_products.ownership_id',
        'products.plu as product_code',
        'products.description as product_name',
        'categories.min_price',
        'categories.max_price',
        'categories.rec_price as suggest_price',
        'user_products.stock',
        'products.unit as unit_igr',
        'user_products.unit as unit_tmi',
        'products.min_order',
        'products.min_qty',
        'products.max_qty',
        'user_products.flag_freeplu',
        'user_products.id as user_product_id',
        'user_products.unit',
        'user_products.cost',
        'user_products.price',
        'ownerships.ownership',
        'products.status_product_master_id',
        'products.conversion',
        'products.frac_igr',
        'products.frac_tmi',
        'user_products.deleted_at')
        ->groupBy('user_products.id')
        ->limit(10)
        ->get();

        // dd($data_product);

        foreach($data_product as $product)
        {
            if($product->product_id != null)
            {
                $barcode = Barcode::where('product_id','=',$product->product_id)
                ->select('barcode')
                ->get();
            }
            else
            {
                $barcode = UserBarcode::where('user_product_id','=',$product->user_product_id)
                ->select('barcode')
                ->get();
            }

            foreach ($barcode as $bar)
            {
                $arr_barcode[] = $bar->barcode;
                // dd($bar->barcode);
            }

            // $array = array("barcode" => $arr_barcode);
            $product['barcode'] = $arr_barcode;
        }

        $data_trx_shift_report = ShiftReport::join('operators','operators.id','=','shift_reports.operator_id')
        ->where('operators.user_id','=',$user_id)
        ->select('shift_reports.id',
        'shift_reports.operator_id',
        'shift_reports.date',
        'shift_reports.shift',
        'shift_reports.notes',
        'shift_reports.beg_balance',
        'shift_reports.end_balance',
        'shift_reports.open_shift',
        'shift_reports.close_shift',
        'shift_reports.sales',
        'shift_reports.sales_amt',
        'shift_reports.margin',
        'shift_reports.created_at',
        'operators.code as operator_code')
        // ->limit(1)
        ->get();

        foreach ($data_trx_shift_report as $data_shift)
        {
            $trx_header = TrxHeader::where('shift_id','=',$data_shift->id)
            ->select('id',
            'user_id',
            'shift_id',
            'payment_method_id',
            'trx_no',
            'payment_reff',
            'qty',
            'price',
            'tax',
            'discount',
            'grand_total',
            'margin',
            'cost',
            'trx_date',
            'created_at')
            // ->limit(3)
            ->get();

            $data_shift['data_trx_header'] = $trx_header;

            foreach($trx_header as $header)
            {
                $trx_detail = TrxDetail::leftjoin('user_products','user_products.id','=','trx_details.user_product_id')
                ->leftjoin('products','products.id','=','user_products.product_id')
                ->leftjoin('promotions','promotions.id','=','trx_details.promotion_id')
                ->where('trx_details.trx_header_id','=',$header->id)
                ->select('trx_details.id',
                'trx_details.trx_header_id',
                'trx_details.user_product_id',
                'trx_details.promotion_id',
                'trx_details.qty',
                'trx_details.price',
                'trx_details.tax',
                'trx_details.discount',
                'trx_details.sub_total',
                'trx_details.margin',
                'trx_details.cost',
                'trx_details.created_at',
                'products.plu',
                'promotions.mechanism')
                // ->limit(3)
                ->get();

                $header['data_trx_details'] = $trx_detail;
            }
        }
        
        return response()->json($data_trx_shift_report);

        $data_ownership = Ownership::all();
        $data_promotion = Promotion::join('user_products','user_products.id','=','promotions.user_product_id')
        ->where('user_products.user_id','=',$user_id)
        ->get();
        $data_pb_status = PBStatus::all();
        $data_operator = Operator::where('user_id','=',$user_id)->get();
        $data_payment_method = PaymentMethod::all();

        $data['data_status_product_master'] = $data_statusProdMast;
        $data['data_product'] = $data_product;
        $data['data_trx_shift_report'] = $data_trx_shift_report;
        $data['data_ownership'] = $data_ownership;
        $data['data_promotion'] = $data_promotion;
        $data['data_pb_status'] = $data_pb_status;
        $data['data_operator'] = $data_operator;
        $data['data_payment_methods'] = $data_payment_method;


        $response['status'] = 1;
        $response['message'] = $data;

        return response()->json($response, 200);
    }

    public function get_user()
    {
        return 'stag';
    }

    public function getTokenAndRefreshToken(OClient $oClient, $email, $password)
    {
        $oClient = OClient::where('password_client',1)->first();
        // dd($oClient->id);
        $http = new Client();
        $response = $http->request('POST','http://172.20.111.95/idm/loginBearer/public/oauth/token',
        [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $email,
                'password' => $password
            ],
        ]);

        $result = json_decode((string) $response->getBody(), true);
        return response()->json($result, 201);
    }

    public function userRefreshToken(Request $request)
    {
        $oClient = OClient::where('password_client',1)->first();

        $http = new Client();
        $response = $http->request('POST','http://192.168.3.114/idm/loginBearer/public/oauth/token',
        [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $request->refresh_token,
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                // 'username' => $email,
                // 'password' => $password
            ],
        ]);

        $result = json_decode((string) $response->getBody(), true);
        return response()->json($result, 201);
    }

    public function sendMail()
    {
        $detail_mail = [
            'title' => 'Email from Sender',
            'body' => 'This is for testing mail using SMTP'
        ];

        Mail::to('ivandanasuta4@gmail.com')->send(new MyTestMail($detail_mail));

        dd("Email is Sent");
    }
}
