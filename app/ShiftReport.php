<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftReport extends Model
{
    //
    protected $table = 'shift_reports';
    protected $primaryKey = 'id';
}
