<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apk extends Model
{
    //
    protected $table = 'apks';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'desc','icon',
        'path_file','size','license',
        'version','created_at','updated_at','deleted_at'
    ];
}
