<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ownership extends Model
{
    //
    protected $table = 'ownerships';
    protected $primaryKey = 'id';
}
