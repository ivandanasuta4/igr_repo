<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PBStatus extends Model
{
    //
    protected $table = 'pb_statuses';
    protected $primaryKey = 'id';
}
